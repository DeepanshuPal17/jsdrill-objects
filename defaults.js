module.exports=function defaults (obj,values)
{
    if(typeof obj==='object')
    {
        for(let i in values)
        {
            if(obj[i]== undefined)
            {
                obj[i]=values[i];
            }
        }
    }
    return obj;
}