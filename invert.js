module.exports=function invert (obj)
{
    if(typeof obj==='object')
    {
        let key={};
        for(let i in obj)
        {
            if(typeof obj[i]!=='function' )
            {
                key[obj[i]]=i;
            }
        }
        return key;
    }
}